后续将基于NextApi改造制作新Api

# Among Us免责声明
> This mod is not affiliated with Among Us or Innersloth LLC, and the content contained therein is not endorsed or otherwise sponsored by Innersloth LLC. Portions of the materials contained herein are property of Innersloth LLC. © Innersloth LLC.

# 许可证声明
> ***本模组遵循GPLv3协议, 允许任何人使用分发代码以及其产物,如引用或修改并传播也请遵循协议进行开源并引用本项目*** 
> 
> ***资源请勿用于任何商业用途***
> 
> ***本模组任何图片音频等资源如有侵权请联系***