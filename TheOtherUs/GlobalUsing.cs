﻿extern alias JetBrains;
global using HarmonyLib;
global using Il2CppInterop.Runtime;
global using Il2CppInterop.Runtime.Attributes;
global using Il2CppInterop.Runtime.Injection;
global using Il2CppInterop.Runtime.InteropTypes;
global using Il2CppInterop.Runtime.InteropTypes.Arrays;
global using TheOtherUs.Helper;
global using TheOtherUs.Utilities;
global using TheOtherUs.Roles;
global using TheOtherUs.Roles.Crewmate;
global using TheOtherUs.Roles.Impostor;
global using TheOtherUs.Roles.Neutral;
global using TheOtherUs.Roles.Modifier;
global using TheOtherUs.Roles.Assigns;
global using TheOtherUs.Modules;
global using static TheOtherUs.Helper.RoleHelper;
global using static TheOtherUs.Roles.CustomRoleManager;
global using static TheOtherUs.Languages.LanguageExtension;
global using static TheOtherUs.Helper.LogHelper;
global using Main = TheOtherUs.TheOtherRolesPlugin;
global using UsedImplicitly = JetBrains::JetBrains.Annotations.UsedImplicitlyAttribute;
global using MeansImplicitUse = JetBrains::JetBrains.Annotations.MeansImplicitUseAttribute;
global using Il2Generic = Il2CppSystem.Collections.Generic;